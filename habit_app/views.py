from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, UpdateView, DeleteView, ListView, CreateView
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissionsOrAnonReadOnly
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from django.shortcuts import redirect
from habit_app.forms import HabitCreateForm
from habit_app.models import Habit
from habit_app.paginators import HabitPagination
from habit_app.permissions import IsOwner
from habit_app.serializers import HabitSerializer
from habit_app.services import create_reminder, update_reminder, delete_reminder
from django.urls import reverse_lazy

from habit_app.validators import HabitValidator


class HabitCreateAPIView(CreateAPIView):
    queryset = Habit.objects.all()
    serializer_class = HabitSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        new_habit = serializer.save()
        new_habit.owner = self.request.user

        create_reminder(new_habit)
        new_habit.save()

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('users:register')
        else:
            return super().dispatch(request, *args, **kwargs)


class HabitListAPIView(ListAPIView):
    queryset = Habit.objects.all()
    serializer_class = HabitSerializer
    pagination_class = HabitPagination
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        """ Определяем параметры вывода объектов """

        queryset = Habit.objects.filter(owner=self.request.user)
        return queryset


class HabitPublicListAPIView(ListAPIView):
    """ Вывод списка публичных привычек """

    serializer_class = HabitSerializer
    pagination_class = HabitPagination
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def get_queryset(self):
        """ Определяем параметры вывода объектов """

        queryset = Habit.objects.filter(is_public=True)
        return queryset


class HabitRetrieveAPIView(RetrieveAPIView):
    queryset = Habit.objects.all()
    serializer_class = HabitSerializer
    permission_classes = [IsAuthenticated, IsOwner]


class HabitUpdateAPIView(UpdateAPIView):
    queryset = Habit.objects.all()
    serializer_class = HabitSerializer
    permission_classes = [IsAuthenticated, IsOwner]


    def perform_update(self, serializer):
        habit = serializer.save()
        update_reminder(habit)
        habit.save()


class HabitDestroyAPIView(DestroyAPIView):
    queryset = Habit.objects.all()
    permission_classes = [IsAuthenticated, IsOwner]

    def perform_destroy(self, instance):
        delete_reminder(instance)
        instance.delete()


class HabitTemplateView(TemplateView):
    template_name = 'habit_app/home.html'
    extra_context = {
            'title': "Главная страница",
    }


class HabitCreateView(LoginRequiredMixin, CreateView):
    model = Habit
    form_class = HabitCreateForm
    permission_classes = [IsAuthenticated]
    success_url = reverse_lazy('habit_app:options_list')
    extra_context = {
            'title': "Создать привычку",
    }

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('users:register')
            # return redirect('habit_app:home')
        else:
            return super().dispatch(request, *args, **kwargs)

    # def form_valid(self, form):
    #     self.object = form.save()
    #     self.object.owner = self.request.user
    #     self.object.save()

    def form_valid(self, form):
        self.object = form.save()
        send_params = form.save()
        self.object.owner = self.request.user

        send_params.save()
        self.object.save()
        create_reminder(send_params)
        return super().form_valid(form)


class HabitOptionsListView(ListView):
    model = Habit
    permission_classes = [IsAuthenticated, IsOwner]
    extra_context = {
            'title': "Все привычки",
    }

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(owner=self.request.user)
        return queryset

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('users:register')
        else:
            return super().dispatch(request, *args, **kwargs)


class HabitOptionsDeleteView(LoginRequiredMixin, DeleteView):
    model = Habit
    success_url = reverse_lazy('habit_app:options_list')
    permission_classes = [IsAuthenticated, IsOwner]


class HabitOptionsUpdateView(LoginRequiredMixin, UpdateView):
    model = Habit
    form_class = HabitCreateForm
    # validators = HabitValidator
    permission_classes = [IsAuthenticated, IsOwner]
    success_url = reverse_lazy('habit_app:options_list')
    extra_context = {
            'title': "Редактирование привычки",
    }

    def form_valid(self, form):
        self.object = form.save()
        send_params = form.save()
        self.object.owner = self.request.user

        send_params.save()
        self.object.save()
        return super().form_valid(form)

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        queryset = queryset.filter(id=self.kwargs.get('pk'))
        return queryset


