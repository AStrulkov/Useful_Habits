import captcha as captcha
from captcha.fields import CaptchaField
from django import forms
from habit_app.models import Habit


# class StyleFormMixin:
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field_name, field in self.fields.items():
#             # field.help_text = 'Some help text for field'
#             if field_name in ['nice_habit', 'is_public']:
#                 field.widget.attrs['class'] = 'form'
#             # elif field_name in ['time_when_execute']:
#             #     field.widget.attrs['class'] = 'Time'
#             else:
#                 field.widget.attrs['class'] = 'form-control'

class HabitCreateForm(forms.ModelForm):
    # captcha = CaptchaField(label='Введите текст ст картинки')

    class Meta:
        model = Habit
        exclude = ('date', 'owner', 'lead_time', 'related_habit', 'name', 'nice_habit', 'is_public')

    def clean(self):

        cleaned_data = super(HabitCreateForm, self).clean()
        related_habit = self.cleaned_data.get("related_habit")
        reward = self.cleaned_data.get("reward")
        nice_habit = self.cleaned_data.get("nice_habit")
        name = self.cleaned_data.get("name")

        if related_habit and reward:
            raise forms.ValidationError(
                'Для полезной привычки нужно указать связанную привычку или '
                'вознаграждение'
                )

        if name == 'Полезная':
            if not related_habit and not reward:
                raise forms.ValidationError(
                        'Для полезной привычки нужно указать связанную привычку или '
                        'вознаграждение'
                )

        if nice_habit:
            if related_habit:
                raise forms.ValidationError('У приятной привычки не может быть связанной привычки')
            elif reward:
                raise forms.ValidationError('У приятной привычки не может быть вознаграждения')

        if name == 'Полезная':
            if nice_habit:
                raise forms.ValidationError('Ели привычка полезная, то должно отсутствовать указание приятной '
                                            'привычки')
        if name == 'Приятная':
            if not nice_habit:
                raise forms.ValidationError('Ели привычка приятная, то должно присутствовать указание приятной '
                                            'привычки')

        if related_habit:
            if not related_habit.nice_habit:
                raise forms.ValidationError(
                    'В связанные привычки могут попадать только привычки с признаком '
                    'приятной привычки'
                    )