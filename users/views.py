from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView
from rest_framework.viewsets import ModelViewSet
from users.forms import RegisterForm, UserProfileForm
from users.models import User
from users.serializers import UserSerializer
from django.urls import reverse_lazy, reverse
from django.contrib.auth.views import LoginView as BaseLoginView
from django.contrib.auth.views import LogoutView as BaseLogoutView


class UsersViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # success_url = reverse_lazy('habit_app:home')

    def perform_create(self, serializer):
        user = serializer.save()
        user.set_password(user.password)
        user.save()


class RegisterView(CreateView):
    model = User
    form_class = RegisterForm
    success_url = reverse_lazy('habit_app:home')
    template_name = 'users/register.html'
    success_message = 'Вы успешно зарегистрировались'


class UserUpdateView(LoginRequiredMixin, UpdateView):
    models = User
    success_url = reverse_lazy('habit_app:home')
    form_class = UserProfileForm

    def get_object(self, queryset=None):
        return self.request.user


class LoginView(BaseLoginView):
    template_name = 'users/login.html'


class LogoutView(BaseLogoutView):
    pass

