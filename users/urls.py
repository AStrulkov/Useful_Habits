from rest_framework import routers, permissions
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

# from django.urls import path

from users.apps import UsersConfig
from users.views import UsersViewSet, LoginView, LogoutView, RegisterView, UserUpdateView
from django.views.generic import TemplateView
from django.urls import path, include

app_name = UsersConfig.name

router = routers.SimpleRouter()
router.register(r'api/users', UsersViewSet, basename='пользователи')

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('accounts/login/', LoginView.as_view(), name='login'),
    path('', LogoutView.as_view(), name='logout'),
    path('accounts/register/', RegisterView.as_view(), name='register'),
    path('accounts/profile/', UserUpdateView.as_view(), name='profile'),

] + router.urls

