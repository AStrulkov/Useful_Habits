from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from captcha.fields import CaptchaField
from habit_app.models import Habit
# from catalog.forms import StyleFormMixin
from users.models import User
# from users.signals import post_register


class StyleFormMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name != 'is_staff':
                field.widget.attrs['class'] = 'form-control'


class UserProfileForm(StyleFormMixin, UserChangeForm):
    """Добавление формы профиля пользователя"""

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password', 'telegram_id')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['password'].widget = forms.HiddenInput()


class RegisterForm(StyleFormMixin, UserCreationForm):
    """Добавление формы регистрации пользователя с верификацией"""
    captcha = CaptchaField(label='Введите текст ст картинки')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'telegram_id')


class HabitForm(StyleFormMixin, forms.ModelForm):
    class Meta:
        model = Habit
        fields = '__all__'
